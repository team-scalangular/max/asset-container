TEXT_COLOUR='\033[0;32m'
NC='\033[0m'
echo "Heya, are you currently working?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) git work; break;;
        No ) git private; break;;
    esac
done

echo "${TEXT_COLOUR}=================================="
echo "${TEXT_COLOUR}Updated gitconfig according to you awnser, have a great time!"
echo "${TEXT_COLOUR}==================================${NC}"

cat ~/.gitconfig

sleep 5
clear
exit
